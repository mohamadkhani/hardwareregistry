from bottle import post, request
import json, sqlite3, sys, traceback, time

import auth

#conn = sqlite3.connect('Shenasname.db')
conn = ""


def _init_(_conn):
    global conn
    conn = _conn


def post_get(name, default=''):
    return request.POST.get(name, default).strip()



@post('/update/Case/<shomareAmval>')
def update(shomareAmval):
    objs = json.loads(request.body.read())
    if 'ip' in objs and objs['ip']:
        c = conn.cursor()
        c.execute("SELECT ip FROM `Case` WHERE ip = '"+objs["ip"]+"'")
        if len(c.fetchall()) > 0:
            return ({'id': -1, 'message': "آی پی تکراری است.", 'status': 'warning'})
    if 'Yegan_id' in objs:
        conn.execute("UPDATE Hardware_Yegan SET `status` = 0 ,`outDate` =" + str(
            time.time()) + " WHERE `Hardware_id`=" + shomareAmval)
        conn.execute("UPDATE `Hardware` SET `status` = 1 WHERE shomareAmval=" + str(shomareAmval))
        conn.execute("""
        INSERT INTO `Hardware_Yegan`
        (`Hardware_id`,`Yegan_id`,`inDate`,`outDate`,`status`) VALUES
        (""" + str(shomareAmval) + """,""" + str(objs['Yegan_id']) + """,""" + str(time.time()) + """,0,1)
        """)
        del objs['Yegan_id']
    if 'Yegan_title' in objs:
        del objs['Yegan_title']
    sets = ""
    first = True
    for k, v in objs.items():
        if first:
            sets += " `" + k + "` = '" + str(v) + "' "
            first = False
        else:
            sets += " , `" + k + "` = '" + str(v) + "' "
    conn.execute("UPDATE `Case` SET " + sets + " WHERE `shomareAmval` = " + str(shomareAmval))

    conn.commit()
    return ({'id': 1, 'message': "ثبت شد.", 'status': 'success'})


@post('/getAll/Case')
def getAll():
    m = json.loads(request.body.read())
    page = m['page']
    row_limit = m['row_limit']
    where = ""
    if auth.aaa.current_user.role == "user":
        v = auth.aaa.current_user.email_addr
        where += " Yegan.id = '" + v + "' OR `Y1`.id = '" + v + "' OR `Y1`.ParentYegan_id = '" + v + "' OR `Y2`.ParentYegan_id = '" + v + "' OR `Y3`.ParentYegan_id = '" + v + "' OR `Y4`.ParentYegan_id = '" + v + "' AND "

    onlyId = False

    if 'onlyId' in m:
        onlyId = True
    if 'filter' in m:
        first = True
        for k, v in m['filter'].items():
            if first:
                where += " `" + k + "` LIKE '" + v + "' "
                first = False
                if k == "Yegan_id":

                    where += " OR `Y1`.id = '" + v + "' OR `Y1`.ParentYegan_id = '" + v + "' OR `Y2`.ParentYegan_id = '" + v + "' OR `Y3`.ParentYegan_id = '" + v + "' OR `Y4`.ParentYegan_id = '" + v + "'"
            else:
                where += " AND `" + k + "` LIKE '" + v + "' "
                if k == "Yegan_id":
                    where += " OR `Y1`.id = '" + v + "' OR `Y1`.ParentYegan_id = '" + v + "' OR `Y2`.ParentYegan_id = '" + v + "' OR `Y3`.ParentYegan_id = '" + v + "' OR `Y4`.ParentYegan_id = '" + v + "'"

    c = conn.cursor()
    _select = """SELECT `Case`.shomareShenasaei,`Case`.shomareAmval,`Yegan`.title,`Yegan`.id,inNaja,internet,
                `CaseType`.title,`CaseType`.id,`MonitorType`.title,`MonitorType`.id,Monitor_shomareAmval,
                `OS`.title,`OS`.id,`CPU`.title,`CPU`.id,`RAM`.title,`RAM`.id,`Harddisk`.title,`Harddisk`.id,mac_address,ip,Printer_shomareAmval,Scanner_shomareAmval,Mainboard_id,Mainboard.title 
                ,`Y2`.title , `Y1`.title """

    tables = """ FROM `Case`
              LEFT JOIN `Hardware` ON `Hardware`.shomareAmval                                                                                                                                                                                                                                                                                             = `Case`.shomareAmval
              LEFT JOIN `Hardware_Yegan` ON `Hardware_Yegan`.Hardware_id = `Case`.shomareAmval AND `Hardware_Yegan`.`status` = 1
              LEFT JOIN `Yegan` ON `Yegan`.id = `Hardware_Yegan`.`Yegan_id`
              LEFT JOIN `Yegan` Y1 ON `Yegan`.ParentYegan_id = Y1.id
              LEFT JOIN `Yegan` Y2 ON `Y1`.`ParentYegan_id` = Y2.id
              LEFT JOIN `Yegan` Y3 ON `Y2`.`ParentYegan_id` = Y3.id
              LEFT JOIN `Yegan` Y4 ON `Y3`.`ParentYegan_id` = Y4.id
              LEFT JOIN `CaseType` ON `CaseType`.id = `Case`.CaseType_id
              LEFT JOIN `Monitor` ON `Monitor`.shomareAmval = `Case`.Monitor_shomareAmval
              LEFT JOIN `MonitorType` ON `MonitorType`.id = `Monitor`.`Type_id`
              LEFT JOIN `OS` ON `OS`.id = `Case`.OS_id
              LEFT JOIN `CPU` ON `CPU`.id = `Case`.CPU_id
              LEFT JOIN `RAM` ON `RAM`.id = `Case`.RAM_id
              LEFT JOIN `Harddisk` ON `Harddisk`.id = `Case`.Harddisk_id
              LEFT JOIN `Mainboard` ON `Mainboard`.id = `Case`.Mainboard_id
                
              """
    query = ""
    if where != "":
        query += "SELECT `Case`.shomareShenasaei " + tables + " WHERE " + where + " GROUP BY `Case`.shomareAmval"
    else:
        query += "SELECT `Case`.shomareShenasaei " + tables + " GROUP BY `Case`.shomareAmval"
    print(query)
    c.execute(query)
    num_rows = len(c.fetchall())
    if where != "":
        query = _select + tables + " WHERE " + where + " GROUP BY `Case`.shomareAmval LIMIT " + \
                str((page - 1) * int(row_limit)) + "," + str(row_limit)
    else:
        query = _select + tables + " GROUP BY `Case`.shomareAmval ORDER BY regDate DESC LIMIT " + \
                str((page - 1) * int(row_limit)) + "," + str(row_limit)
    print(query)
    c.execute(query)
    qResults = c.fetchall()
    results = []
    for qResult in qResults:
        result = {}
        result["shomareShenasaei"] = str(qResult[0])
        result["shomareAmval"] = str(qResult[1])
        result["Yegan_title"] = qResult[2]
        result["Yegan_id"] = str(qResult[3])
        result["inNaja"] = str(qResult[4])
        result["internet"] = str(qResult[5])
        if not onlyId: result["CaseType_title"] = qResult[6]
        result["CaseType_id"] = str(qResult[7])
        if not onlyId: result["MonitorType_title"] = qResult[8]
        if not onlyId: result["MonitorType_id"] = str(qResult[9])
        if qResult[10]: result["Monitor_shomareAmval"] = str(qResult[10])
        if not onlyId: result["OS_title"] = qResult[11]
        result["OS_id"] = str(qResult[12])
        if not onlyId: result["CPU_title"] = qResult[13]
        result["CPU_id"] = str(qResult[14])
        if not onlyId: result["RAM_title"] = qResult[15]
        result["RAM_id"] = str(qResult[16])
        if not onlyId: result["Harddisk_title"] = qResult[17]
        result["Harddisk_id"] = str(qResult[18])
        result["mac_address"] = qResult[19]
        result["ip"] = qResult[20]
        if qResult[21]: result["Printer_shomareAmval"] = str(qResult[21])
        if qResult[22]: result["Scanner_shomareAmval"] = str(qResult[22])
        result["Mainboard_id"] = str(qResult[23])
        if not onlyId: result["Mainboard_title"] = str(qResult[24])
        if not onlyId: result["Yegan2_title"] = str(qResult[25])
        if not onlyId: result["Yegan3_title"] = str(qResult[26])

        results.append(result)
    return json.dumps({"rows": results, 'num_rows': num_rows})


@post('/register/Case')  # or @route('/login', method='POST')
def register():
    objs = json.loads(request.body.read())
    if 'ip' in objs and objs['ip']:
        c = conn.cursor()
        c.execute("SELECT ip FROM `Case` WHERE ip = '"+objs["ip"]+"'")
        if len(c.fetchall()) > 0:
            return ({'id': -1, 'message': "آی پی تکراری است.", 'status': 'warning'})
    c = conn.cursor()
    if 'Monitor_shomareAmval' in objs and objs['Monitor_shomareAmval'] != '':
        c.execute("SELECT * FROM `Monitor` WHERE `shomareAmval` =" + str(objs['Monitor_shomareAmval']) + "")
        if len(c.fetchall()) != 1:
            return {'id': -4, 'message': "مانیتور با شماره اموال زیر یافت نشد.", 'status': 'error'}

    c.execute("SELECT * FROM `Hardware` WHERE `shomareAmval` = " + str(objs['shomareAmval']) + "")
    if len(c.fetchall()) == 1:
        return {'id': -5, 'message': "شماره اموال در سامانه موجود است.", 'status': 'error'}

    if 'ip' in objs:
        c.execute("SELECT * FROM `Case` WHERE `ip` = '" + str(objs['ip']) + "'")
        if len(c.fetchall()) == 1:
            return {'id': -5, 'message': "سیستمی با این IP در سامانه موجود است.", 'status': 'error'}

    try:
        if "description" not in objs.keys():
            objs['description'] = ""
        conn.execute("""INSERT INTO `Hardware` (`shomareAmval`,`shomareShenasaei`,`type`,`regDate`,`status`,`description`) 
        VALUES(""" + str(objs['shomareAmval']) + """,""" + str(objs['shomareShenasaei']) + """,1,""" + str(
            time.time()) + """,1,'""" + str(objs["description"]) + """');""")
        if "samane" not in objs.keys():
            objs['samane'] = ""
        if "ip" not in objs.keys():
            objs['ip'] = ""
        if "mac_address" not in objs.keys():
            objs['mac_address'] = ""

        conn.execute("""
        INSERT INTO `Hardware_Yegan`
        (`Hardware_id`,`Yegan_id`,`inDate`,`outDate`,`status`) VALUES
        (""" + str(objs['shomareAmval']) + """,""" + str(objs['Yegan_id']) + """,""" + str(time.time()) + """,0,1)
        """)
        if 'Monitor_shomareAmval' not in objs.keys() or objs['Monitor_shomareAmval'] == "":
            objs['Monitor_shomareAmval'] = "Null"
        if 'Printer_shomareAmval' not in objs.keys() or objs['Printer_shomareAmval'] == "":
            objs['Printer_shomareAmval'] = "Null"
        if 'Scanner_shomareAmval' not in objs.keys() or objs['Scanner_shomareAmval'] == "":
            objs['Scanner_shomareAmval'] = "Null"

        conn.execute("""
                 INSERT INTO `Case`
                 (
                 shomareShenasaei,shomareAmval,inNaja,internet,CaseType_id,Monitor_shomareAmval,OS_id,CPU_id,RAM_id,mac_address,ip,Harddisk_id,samane,Printer_shomareAmval,Scanner_shomareAmval,Mainboard_id
                 ) Values
                 (
                 '""" + str(objs['shomareShenasaei']) + """',
                 '""" + str(objs['shomareAmval']) + """',
                 '""" + str(objs['inNaja']) + """',
                 '""" + str(objs['internet']) + """',
                 '""" + str(objs['CaseType_id']) + """',
                 """ + str(objs['Monitor_shomareAmval']) + """,
                 '""" + str(objs['OS_id']) + """',
                 '""" + str(objs['CPU_id']) + """',
                 '""" + str(objs['RAM_id']) + """',
                 '""" + str(objs['mac_address']) + """',
                 '""" + str(objs['ip']) + """',
                 '""" + str(objs['Harddisk_id']) + """',
                 '""" + str(objs['samane']) + """',
                 """ + str(objs['Printer_shomareAmval']) + """,
                 """ + str(objs['Scanner_shomareAmval']) + """,
                 """ + str(objs['Mainboard_id']) + """
                 )""")

    except:
        conn.rollback()
        a, b, c = sys.exc_info()
        errr = traceback.format_exception(a, b, c)
        for d in errr:
            if str(d).find("sqlite3.IntegrityError: UNIQUE constraint failed:") >= 0:
                l = d.replace("sqlite3.IntegrityError: UNIQUE constraint failed: ", "")
                if l[l.find(".") + 1:l.find(" ")] == "shomareShenasaei":
                    return {'id': -2, 'message': "شماره شناسایی قبلا در سیستم ثبت شده. ", 'status': 'error'}
                else:
                    if l[l.find(".") + 1:l.find(" ")] == "shomareAmval":
                        return {'id': -3, 'message': "شماره اموال قبلا در سیستم ثبت شده. ", 'status': 'error'}
                    else:
                        if l[l.find(".") + 1:l.find(" ")] == "Monitor_shomareAmval":
                            return {'id': -6, 'message': "مانیتور برای کیس دیگری ثبت شده. ", 'status': 'error'}
                        else:
                            return {'id': -1, 'message': "خطا ناشناخته رخ داده.. ", 'status': 'error'}
        return {'id': -7, 'message': errr, 'status': 'error'}
    conn.commit()
    return {'id': 1, 'message': "ثبت شد.", 'status': 'success'}


@post("/get/Case")
def get_case():
    objs = json.loads(request.body.read())
    query = """SELECT `Case`.shomareShenasaei,`Case`.shomareAmval,`Yegan`.title,`Yegan`.id,inNaja,internet,
                `CaseType`.title,`CaseType`.id,`MonitorType`.title,`MonitorType`.id,Monitor_shomareAmval,
                `OS`.title,`OS`.id,`CPU`.title,`CPU`.id,`RAM`.title,`RAM`.id,`Harddisk`.title,`Harddisk`.id,mac_address,ip,Printer_shomareAmval,Scanner_shomareAmval 
                FROM `Case`
              LEFT JOIN `Hardware` ON `Hardware`.shomareAmval = `Case`.shomareAmval
              LEFT JOIN `Hardware_Yegan` ON `Hardware_Yegan`.Hardware_id = `Case`.shomareAmval AND `Hardware_Yegan`.`status` = 1
              LEFT JOIN `Yegan` ON `Yegan`.id = `Hardware_Yegan`.`Yegan_id`
              LEFT JOIN `CaseType` ON `CaseType`.id = `Case`.CaseType_id
              LEFT JOIN `Monitor` ON `Monitor`.shomareAmval = `Case`.Monitor_shomareAmval
              LEFT JOIN `MonitorType` ON `MonitorType`.id = `Monitor`.`Type_id`
              LEFT JOIN `OS` ON `OS`.id = `Case`.OS_id
              LEFT JOIN `CPU` ON `CPU`.id = `Case`.CPU_id
              LEFT JOIN `RAM` ON `RAM`.id = `Case`.RAM_id
              LEFT JOIN `Harddisk` ON `Harddisk`.id = `Case`.Harddisk_id
              WHERE `Case`.shomareShenasaei = """ + str(objs['shomareShenasaei']) + """ OR `Case`.shomareAmval = """ + str(objs[
        'shomareShenasaei']) + """
              """
    c = conn.cursor()
    c.execute(query)
    qResults = c.fetchall()
    print(qResults)
    if len(qResults) == 0:
        return {'id': -1, 'message': "سیستمی با این شماره شناسایی یافت نشد.", 'status': 'warning'}
    qResult = qResults[0]
    onlyId = False
    result = {}
    result["shomareShenasaei"] = str(qResult[0])
    result["shomareAmval"] = str(qResult[1])
    result["Yegan_title"] = qResult[2]
    if not onlyId: result["Yegan_id"] = str(qResult[3])
    result["inNaja"] = str(qResult[4])
    result["internet"] = str(qResult[5])
    if not onlyId: result["CaseType_title"] = qResult[6]
    result["CaseType_id"] = str(qResult[7])
    if not onlyId: result["MonitorType_title"] = qResult[8]
    if not onlyId: result["MonitorType_id"] = str(qResult[9])
    if qResult[10]:result["Monitor_shomareAmval"] = str(qResult[10])
    if not onlyId: result["OS_title"] = qResult[11]
    result["OS_id"] = str(qResult[12])
    if not onlyId: result["CPU_title"] = qResult[13]
    result["CPU_id"] = str(qResult[14])
    if not onlyId: result["RAM_title"] = qResult[15]
    result["RAM_id"] = str(qResult[16])
    if not onlyId: result["Harddisk_title"] = qResult[17]
    result["Harddisk_id"] = str(qResult[18])
    result["mac_address"] = qResult[19]
    result["ip"] = qResult[20]
    if qResult[21]: result["Printer_shomareAmval"] = str(qResult[21])
    if qResult[22]: result["Scanner_shomareAmval"] = str(qResult[22])
    return json.dumps(result)
