from bottle import post, request
import json, sqlite3

def _init_(_conn):
    global conn
    conn = _conn


def getAll(entity):
    c = conn.cursor()
    c.execute("SELECT * FROM `" + entity + "`")
    return c.fetchall()


@post('/getAll/Yegan')
def get_yegan():
    yegans = {}
    for yegan1 in getAll("Yegan"):
        yegan = {'id': yegan1[0], 'title': yegan1[1], 'ParentYegan_id': yegan1[2], 'r': yegan1[3]}
        yegans[yegan['id']] = yegan
    return json.dumps(yegans)


@post('/delete/Yegan')
def del_yegan():
    m = json.loads(request.body.read())
    c = conn.cursor()
    c.execute("SELECT id FROM `Yegan` WHERE ParentYegan_id=" + str(m['id']))
    if len(c.fetchall()) == 0:
        c.execute("SELECT id FROM Hardware_Yegan WHERE Yegan_id=" + str(m['id']))
        if len(c.fetchall()) == 0:
            conn.execute("DELETE FROM Yegan WHERE id=" + str(m['id']))
            conn.commit()
            return {'id': 1, 'message': "حذف شد.", 'status': 'success'}
        else:
            return {'id': -1, 'message': "حذف نشد. سخت افزار با این یگان موجود است.", 'status': 'warning'}
    else:
        return {'id': -1, 'message': "حذف نشد. زیرمجموعه وجود دارد.", 'status': 'warning'}


@post('/update/Yegan')
def update_yegan():
    m = json.loads(request.body.read())
    conn.execute("UPDATE Yegan SET title = '" + str(m['title']) + "' WHERE id=" + str(m['id']))
    conn.commit()
    return {'id': 1, 'message': "ثبت شد.", 'status': 'success'}


@post('/register/Yegan')
def register():
    m = json.loads(request.body.read())
    c =[];
    if m["ParentYegan_id"] == None:
        c = conn.execute(
            "INSERT INTO Yegan (title,r,priority) VALUES ('" + str(m["title"]) + "' ," + str(m["r"]) + "," + str(
                m["priority"]) + ")")
    else:
        c = conn.execute(
            "INSERT INTO Yegan (title, ParentYegan_id,r,priority) VALUES ('" + str(m["title"]) + "' ," + str(
                m["ParentYegan_id"]) + "," + str(m["r"]) + "," + str(m["priority"]) + ")")
    conn.commit()

    return {'id': 1, 'message': "ثبت شد.", 'status': 'success', 'regId': c.lastrowid}
