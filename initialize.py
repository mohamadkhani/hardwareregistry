import sqlite3,sys
try:

    conn = sqlite3.connect('Shenasname.db')

    conn.execute("""
        CREATE TABLE `Hardware` (
                `shomareAmval` INTEGER NOT NULL UNIQUE,
                `shomareShenasaei` INTEGER,
                `type` INTEGER,
                `regDate` INTEGER,
                `status` INTEGER,
                `description`	TEXT,
                `Model_id`  INTEGER,
                PRIMARY KEY(`shomareAmval`)
        );
    """)
    conn.execute("""CREATE TABLE `Mainboard`
        (`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
        `title`	TEXT NOT NULL,
        `priority`	INTEGER NOT NULL,
        `type` INTEGER);
    """)

    conn.execute("""CREATE TABLE `Model`
            (`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
            `title`	TEXT NOT NULL,
            `priority`	INTEGER NOT NULL,
            `type` INTEGER);
        """)


    # type 1:case 2:monitor 3:printer 4:scanner 5:UPS
    conn.execute(""" 
        CREATE TABLE `Case` (
                `shomareShenasaei`	INTEGER NOT NULL UNIQUE,
                `shomareAmval`	INTEGER NOT NULL UNIQUE,
                `inNaja`	INTEGER NOT NULL DEFAULT -1,
                `internet`	INTEGER NOT NULL,
                `CaseType_id`	INTEGER NOT NULL,
                `Monitor_shomareAmval`	INTEGER,
                `OS_id`	INTEGER NOT NULL,
                `CPU_id`	INTEGER NOT NULL,
                `RAM_id`	INTEGER NOT NULL,
                `mac_address`	TEXT,
                `ip`	TEXT,
                `Harddisk_id`	INTEGER NOT NULL,
                `samane`	TEXT,
                `‍Scanner_shomareAmval` INTEGER,
                `Printer_shomareAmval` INTEGER,
                `Mainboard_id` INTEGER,
                PRIMARY KEY(`shomareShenasaei`)
    ); """)

    conn.execute("""
        CREATE TABLE `Hardware_Yegan` (
                `id` INTEGER NOT NULL UNIQUE PRIMARY KEY AUTOINCREMENT,
                `Hardware_id` INTEGER,
                `Yegan_id` INTEGER,
                `inDate` INTEGER,
                `outDate` INTEGER,
                `status` INTEGER
        );             
    """)
    # status 1:inYegan 2:returned 3:recycled

    conn.execute("""create table Repair
    (
      id                INTEGER not null
        primary key
                                autoincrement,
      Case_shomareAmval INTEGER not null,
      description       TEXT,
      CPU               INTEGER default NULL,
      CPU_id            INTEGER,
      Harddisk          INTEGER default NULL,
      Harddisk_id       INTEGER,
      OS                INTEGER default NULL,
      OS_id             INTEGER,
      RAM               INTEGER default NULL,
      RAM_id            INTEGER,
      antivirus         INTEGER,
      setIP             INTEGER default NULL,
      ip                INTEGER,
      software          INTEGER,
      inDate            INTEGER,
      outDate           INTEGER,
      deliverer         TEXT,
      getter            TEXT,
      Mainboard_id      INTEGER,
      Mainboard         INTEGER,
      power             INTEGER,
      readyDate         INTEGER,
      Repairer_id       INTEGER,
      plumpNum          INTEGER
    );
    
    """)

    conn.execute(""" 
        CREATE TABLE `Yegan` (
            `id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            `title`	TEXT NOT NULL,
            `ParentYegan_id` INTEGER,
            `r` INTEGER,
            `priority` INTEGER
        );
    """)

    conn.execute("INSERT INTO `Yegan` (`title`,`ParentYegan_id`,`r`,`priority`) VALUES ('فاوا',null,0,1);")
    conn.execute("INSERT INTO `Yegan` (`title`,`ParentYegan_id`,`r`,`priority`) VALUES ('حفاظت',null,0,2);")
    conn.execute("INSERT INTO `Yegan` (`title`,`ParentYegan_id`,`r`,`priority`) VALUES ('فناوری',1,1,3);")
    conn.execute("INSERT INTO `Yegan` (`title`,`ParentYegan_id`,`r`,`priority`) VALUES ('حفاظت',2,1,1);")
    conn.execute("INSERT INTO `Yegan` (`title`,`ParentYegan_id`,`r`,`priority`) VALUES ('فناوری',3,2,1);")
    conn.execute("INSERT INTO `Yegan` (`title`,`ParentYegan_id`,`r`,`priority`) VALUES ('فناوری۱۱',5,3,1);")
    conn.execute("INSERT INTO `Yegan` (`title`,`ParentYegan_id`,`r`,`priority`) VALUES ('ارتباطات',1,1,1);")

    conn.execute(
        """CREATE TABLE `CaseType` (
      `id`       INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
      `title`    TEXT    NOT NULL,
      `priority` INTEGER NOT NULL
    ); """)

    conn.execute("INSERT INTO `CaseType` (`title`,`priority`) VALUES ('PC',1);")
    conn.execute("INSERT INTO `CaseType` (`title`,`priority`) VALUES ('TC',2);")
    conn.execute("INSERT INTO `CaseType` (`title`,`priority`) VALUES ('Laptop',3);")
    conn.execute("INSERT INTO `CaseType` (`title`,`priority`) VALUES ('Netbook',4);")

    conn.execute("""CREATE TABLE `Monitor` (
	`shomareAmval`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`Type_id`	INTEGER NOT NULL,
	`Size`	INTEGER NOT NULL
);""")

    conn.execute("""CREATE TABLE `MonitorType` (`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,`title` TEXT NOT NULL,
    `priority` INTEGER NOT NULL
    ); """)

    conn.execute(
        "CREATE TABLE `PrinterType` (`id`	INTEGER PRIMARY KEY AUTOINCREMENT,`title`	TEXT,`priority`	INTEGER);")
    conn.execute(
        "CREATE TABLE `ScannerType` (`id`	INTEGER PRIMARY KEY AUTOINCREMENT,`title`	TEXT,`priority`	INTEGER);")

    conn.execute("INSERT INTO `MonitorType` (`title`,`priority`) VALUES ('CRT',3);")
    conn.execute("INSERT INTO `MonitorType` (`title`,`priority`) VALUES ('LCD',2);")
    conn.execute("INSERT INTO `MonitorType` (`title`,`priority`) VALUES ('LED',1);")

    conn.execute("""CREATE TABLE `OS` (
      `id`       INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
      `title`    TEXT    NOT NULL,
      `priority` INTEGER NOT NULL
    ); """)

    conn.execute("INSERT INTO `OS` (`title`,`priority`) VALUES ('XP',2);")
    conn.execute("INSERT INTO `OS` (`title`,`priority`) VALUES ('Win7',1);")
    conn.execute("INSERT INTO `OS` (`title`,`priority`) VALUES ('Win8',3);")
    conn.execute("INSERT INTO `OS` (`title`,`priority`) VALUES ('Win10',4);")

    conn.execute("""CREATE TABLE `CPU` (
      `id`       INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
      `title`    TEXT    NOT NULL,
      `priority` INTEGER NOT NULL
    ); """)

    conn.execute("INSERT INTO `CPU` (`title`,`priority`) VALUES ('Pentium',3);")
    conn.execute("INSERT INTO `CPU` (`title`,`priority`) VALUES ('DualCore',1);")
    conn.execute("INSERT INTO `CPU` (`title`,`priority`) VALUES ('Core2Duo',4);")
    conn.execute("INSERT INTO `CPU` (`title`,`priority`) VALUES ('Core i3',2);")
    conn.execute("INSERT INTO `CPU` (`title`,`priority`) VALUES ('Core i5',5);")
    conn.execute("INSERT INTO `CPU` (`title`,`priority`) VALUES ('Core i7',6);")

    conn.execute("""CREATE TABLE `RAM` (
      `id`       INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
      `title`    TEXT    NOT NULL,
      `priority` INTEGER NOT NULL
    ); """)

    conn.execute("INSERT INTO `RAM` (`title`,`priority`) VALUES ('512MB',7);")
    conn.execute("INSERT INTO `RAM` (`title`,`priority`) VALUES ('1GB',4);")
    conn.execute("INSERT INTO `RAM` (`title`,`priority`) VALUES ('2GB',2);")
    conn.execute("INSERT INTO `RAM` (`title`,`priority`) VALUES ('3GB',5);")
    conn.execute("INSERT INTO `RAM` (`title`,`priority`) VALUES ('4GB',1);")
    conn.execute("INSERT INTO `RAM` (`title`,`priority`) VALUES ('6GB',6);")
    conn.execute("INSERT INTO `RAM` (`title`,`priority`) VALUES ('8GB',3);")

    conn.execute("""CREATE TABLE `Harddisk` (
      `id`       INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
      `title`    TEXT    NOT NULL,
      `priority` INTEGER NOT NULL
    ); """)

    conn.execute("INSERT INTO `Harddisk` (`title`,`priority`) VALUES ('40GB',6);")
    conn.execute("INSERT INTO `Harddisk` (`title`,`priority`) VALUES ('80GB',5);")
    conn.execute("INSERT INTO `Harddisk` (`title`,`priority`) VALUES ('160GB',3);")
    conn.execute("INSERT INTO `Harddisk` (`title`,`priority`) VALUES ('300GB',2);")
    conn.execute("INSERT INTO `Harddisk` (`title`,`priority`) VALUES ('500GB',1);")
    conn.execute("INSERT INTO `Harddisk` (`title`,`priority`) VALUES ('1TB',4);")
    conn.execute("INSERT INTO `Harddisk` (`title`,`priority`) VALUES ('2TB',7);")

    conn.execute("""CREATE TABLE `Repairer` (
          `id`       INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
          `title`    TEXT    NOT NULL,
          `priority` INTEGER NOT NULL
        ); """)

    conn.commit()



    print("Initialize completed.")
    input()
except:
    print("Unexpected error:", sys.exc_info())
    input()

