import os
import sys
import json
import sqlite3

from bottle import route, post, static_file,request
from cork import Cork

from cork.backends import SQLiteBackend
import logging
#import canister

conn = sqlite3.connect('auth.db')
logging.basicConfig(format='localhost - - [%(asctime)s] %(message)s', level=logging.DEBUG)
log = logging.getLogger(__name__)




if getattr(sys, 'frozen', False):
    bundle_dir = sys._MEIPASS
else:
    bundle_dir = os.path.dirname(os.path.abspath(__file__))


def populate_backend():
    b = SQLiteBackend('auth.db')
    # b.connection.executescript("""
    #     INSERT INTO users (username, email_addr, desc, role, hash, creation_date) VALUES
    #     (
    #         'admin',
    #         'admin@localhost.local',
    #         'admin test user',
    #         'admin',
    #         'cLzRnzbEwehP6ZzTREh3A4MXJyNo+TV8Hs4//EEbPbiDoo+dmNg22f2RJC282aSwgyWv/O6s3h42qrA6iHx8yfw=',
    #         '2012-10-28 20:50:26.286723'
    #     );
    #     INSERT INTO roles (role, level) VALUES ('special', 200);
    #     INSERT INTO roles (role, level) VALUES ('admin', 100);
    #     INSERT INTO roles (role, level) VALUES ('editor', 60);
    #     INSERT INTO roles (role, level) VALUES ('user', 50);
    # """)
    return b
b = populate_backend()

aaa = Cork(backend=b, email_sender="email@email.em")


def post_get(name, default=''):
    return request.POST.get(name, default).strip()


@route('/login')
def open_login_page():
    return static_file('files/login.html', bundle_dir)

@route('/login')
def open_login_page():
    return static_file('files/login.html', bundle_dir)


@post('/login')
def login():
    username = post_get('username')
    password = post_get('password')
    aaa.login(username, password, success_redirect='/', fail_redirect='/login')

@route('/logout')
def logout():
    aaa.logout(success_redirect='/login')


@post('/register')
def register():
    """Send out registration email"""
    aaa.register(post_get('username'), post_get('password'), "0")
    return 'Please check your mailbox.'


@post('/User/register')
def register_user():
    m = json.loads(request.body.read())
    if "Yegan_id" not in m:
        m["Yegan_id"] = ""
    aaa.create_user(m["username"], m["role"], m["password"], email_addr=m["Yegan_id"], description=m["complete_name"])
    return {'id': 1, 'message': "کاربر جدید ثبت شد.", 'status': 'success'}


@post('/User/updatePass')
def update_pass():
    m = json.loads(request.body.read())
    if "role" not in m:
        m["role"] = None
    if "password" not in m:
        m["password"] = None
    if "Yegan_id" not in m:
        m["Yegan_id"] = None
    aaa.user(m["username"]).update(m["role"], m["password"], email_addr=m["Yegan_id"])
    #h = h.decode('ascii')
    #un = post_get('username')
    #a = aaa.user(un)
    return m




@post('/User/delete')
def delete_user():
    m = json.loads(request.body.read())
    aaa.delete_user(m["username"])
    return {'id': 1, 'message': "کاربر با موفقیت حذف شد.", 'status': 'success'}


#TODO: disable User
#TODO change User password

@route('/User/getAll')
def get_all_users():
    c = conn.cursor()
    c.execute("SELECT * FROM users")
    return json.dumps(c.fetchall())


@route('/User/admin')
def admin():
    """Only admin users can see this"""
    aaa.require(role='admin', fail_redirect='/sorry_page')
    return dict(
        current_user=aaa.current_user
    )



@route('/User/get_current')
def get_current_user():
    return aaa.current_user


@route('/validate_registration/:registration_code')
def validate_registration(registration_code):
    """Validate registration, create user account"""
    aaa.validate_registration(registration_code)
    return 'Thanks. <a href="/login">Go to login</a>'

