#!./venv/bin/python
import json
from beaker.middleware import SessionMiddleware
from cork import Cork
import os
import sqlite3
import sys
import traceback
import bottle

from beaker.middleware import SessionMiddleware


if getattr(sys, 'frozen', False):
    bundle_dir = sys._MEIPASS
else:
    bundle_dir = os.path.dirname(os.path.abspath(__file__))

# os.chdir("/home/fava/Programming/Shenasname/")

try:
    app = bottle.app()
    from bottle import route, run, template, static_file, post, request
    import case, yegan, monitor, hardware, repair, auth

    session_opts = {
        'session.cookie_expires': True,
        'session.encrypt_key': 'please use a random key and keep it secret!',
        'session.httponly': True,
        'session.timeout': 3600 * 24,  # 1 day
        'session.type': 'cookie',
        'session.validate_key': True,
    }
    app = SessionMiddleware(app, session_opts)

    conn = sqlite3.connect('Shenasname.db')
    case._init_(conn)
    yegan._init_(conn)
    monitor._init_(conn)
    hardware._init_(conn)
    repair._init_(conn)

    @post('/register/<entity>')  # or @route('/login', method='POST')
    def register(entity):
        objs = json.loads(request.body.read())
        for k in objs.keys():
            objs[k] = "'" + str(objs[k]) + "'"
        print(objs)
        try:
            print("INSERT INTO `" + entity + """` 
                                 (
                                 """ + ", ".join(objs.keys()) + """
                                 ) 
                                 VALUES 
                                 (
                                        """ + ", ".join(objs.values()) + """       
                                 )
                                 """)
            conn.execute("INSERT INTO `" + entity + """` 
                     (
                     """ + ", ".join(objs.keys()) + """
                     ) 
                     VALUES 
                     (
                            """ + ", ".join(objs.values()) + """       
                     )
                     """)

            conn.commit()
            return {'id': 1, 'message': "ثبت شد.", 'status': 'success'}
        except:
            a, b, c = sys.exc_info()
            for d in traceback.format_exception(a, b, c):
                if str(d).find("sqlite3.IntegrityError: UNIQUE constraint failed:") >= 0:
                    n = d.replace("sqlite3.IntegrityError: UNIQUE constraint failed: ", "")
                    if n[n.find(".") + 1:n.find(" ")] == "id":
                        return {'id': -2, 'message': "شماره وارد شده قبلا در سیستم ثبت شده. ", 'status': 'error'}
                    else:
                        return {'id': -1, 'message': "خطا ناشناخته رخ داده. ", 'status': 'error'}
            return {'id': -1, 'message': "خطا ناشناخته رخ داده. ", 'status': 'error'}


    def get_all(entity):
        c = conn.cursor()
        c.execute("SELECT * FROM `" + entity + "`")
        return c.fetchall()


    def get_all_sorted(entity):
        c = conn.cursor()
        c.execute("SELECT * FROM `" + entity + "` ORDER BY priority ")
        return c.fetchall()


    @route('/getAll/<entity>', method='POST')
    def get_all_json(entity):
        return json.dumps(get_all(entity))


    @route('/getAllSorted/<entity>', method='POST')
    def get_all_json_sorted(entity):
        return json.dumps(get_all_sorted(entity))


    @route('/getAllSorted/Model', method='POST')
    def get_all_json_model_sorted():
        m = json.loads(request.body.read())
        c = conn.cursor()
        c.execute("SELECT * FROM Model WHERE type="+str(m['type'])+" ORDER BY priority ")
        return json.dumps(c.fetchall())


    #@route('/getAll/Yegan', method='POST')
    def get_all_yegan_sorted():
        c = conn.cursor()
        v = auth.aaa.current_user.email_addr
        if auth.aaa.current_user.role == "user":
            c.execute("""SELECT * FROM Yegan 
              LEFT JOIN `Yegan` Y1 ON `Yegan`.ParentYegan_id = Y1.id
              LEFT JOIN `Yegan` Y2 ON `Y1`.`ParentYegan_id` = Y2.id
              LEFT JOIN `Yegan` Y3 ON `Y2`.`ParentYegan_id` = Y3.id
              LEFT JOIN `Yegan` Y4 ON `Y3`.`ParentYegan_id` = Y4.id WHERE """+" Yegan.id = '" + v + "' OR `Y1`.id = '" + v + "' OR `Y1`.ParentYegan_id = '" + v + "' OR `Y2`.ParentYegan_id = '" + v + "' OR `Y3`.ParentYegan_id = '" + v + "' OR `Y4`.ParentYegan_id = '" + v + "'  ORDER BY title DESC")
        else:
            c.execute("SELECT * FROM Yegan ORDER BY title DESC ")
        return c.fetchall()

    @route('/updateSorts/<entity>', method='POST')
    def update_sorts(entity):
        m = json.loads(request.body.read())
        i = 0
        for item in m:
            conn.execute("UPDATE `" + entity + "` SET `priority` = " + str(i) + " WHERE id=" + str(item['id']))
            i += 1


    @route('/delete/<entity>', method='POST')
    def delete(entity):
        print(json.loads(request.body.read()))
        conn.execute("DELETE FROM `" + entity + "` WHERE id = " + str(json.loads(request.body.read())['id']))
        conn.commit()
        return {'id': 1, 'message': "حذف شد.", 'status': 'success'}


    @route('/page/<name>')
    def page(name):
        return static_file(name, bundle_dir + '/pages')


    @route('/favicon.ico')
    def fav():
        return static_file("favicon.ico", bundle_dir + '/files')


    @route('/files/fonts/<name>')
    def font(name):
        return static_file(name, bundle_dir + '/files/fonts')


    @route('/files/<name>')
    def static__file(name):
        # return name
        return static_file('files/' + name, bundle_dir)


    @route('/')
    def index():
        auth.aaa.require(fail_redirect='/login')
        yegans = {}
        for yegan in get_all_yegan_sorted():
            yegans[yegan[0]] = {'id': yegan[0], 'title': yegan[1], 'ParentYegan_id': yegan[2], 'r': yegan[3]}
        yegans = json.dumps(yegans)
        cpus = []
        for cpu in get_all_sorted("cpu"):
            cpus.append({'id': cpu[0], 'title': cpu[1]})
        CaseTypes = []
        for CaseType in get_all_sorted("CaseType"):
            CaseTypes.append({'id': CaseType[0], 'title': CaseType[1]})
        MonitorTypes = []
        for MonitorType in get_all_sorted("MonitorType"):
            MonitorTypes.append({'id': MonitorType[0], 'title': MonitorType[1]})
        oss = []
        for os in get_all_sorted("os"):
            oss.append({'id': os[0], 'title': os[1]})
        rams = []
        for cpu1 in get_all_sorted("ram"):
            rams.append({'id': cpu1[0], 'title': cpu1[1]})
        harddisks = []
        for hdd in get_all_sorted("harddisk"):
            harddisks.append({'id': hdd[0], 'title': hdd[1]})
        mainboards = []
        for mainboard in get_all_sorted("Mainboard"):
            mainboards.append({'id': mainboard[0], 'title': mainboard[1]})
        repairers = []
        for repairer in get_all_sorted("Repairer"):
            repairers.append({'id': repairer[0], 'title': repairer[1]})
        if auth.aaa.current_user:
            current_user = auth.aaa.current_user
        return template(bundle_dir + "/files/index.html", yegans=yegans, cpus=cpus, harddisks=harddisks, oss=oss,
                        rams=rams,
                        MonitorTypes=MonitorTypes, CaseTypes=CaseTypes,
                        mainboards=mainboards, repairers=repairers, current_user=current_user)






    run(app=app,host='0.0.0.0', port=80, debug=True)
except:
    print("Unexpected error:", sys.exc_info())
    input()

def post_get(name, default=''):
    return request.POST.get(name, default).strip()

