from bottle import route,post,request
import json,sqlite3,sys,traceback,time
def _init_(_conn):
    global conn
    conn = _conn


@post('/register/monitor') # or @route('/login', method='POST')
def register():
    objs = json.loads(request.body.read())
    if "description" not in objs.keys():
            objs['description'] = ""
    c = conn.cursor()
    c.execute("SELECT * FROM `Hardware` WHERE `shomareAmval` = "+objs['shomareAmval']+"")
    if len(c.fetchall())==1:
        return({'id':-5,'message':"شماره اموال در سامانه موجود است.",'status':'error'})
        
    conn.execute("""INSERT INTO `Hardware` (`shomareAmval`,`type`,`regDate`,`status`,`description`) 
        VALUES("""+objs['shomareAmval']+""",2,"""+str(time.time())+""",1,'"""+objs['description']+"""');""")
    
    conn.execute("""
        INSERT INTO `Hardware_Yegan`
        (`Hardware_id`,`Yegan_id`,`inDate`,`outDate`,`status`) VALUES
        ("""+objs['shomareAmval']+""","""+objs['Yegan_id']+""","""+str(time.time())+""",0,1)
        """)
    try:
        conn.execute("""
                 INSERT INTO `Monitor`
                 (
                 shomareAmval,Type_id,size
                 ) Values
                 (
                 '"""+objs['shomareAmval']+"""',
                 '"""+objs['Type_id']+"""',
                 '"""+objs['size']+"""'
                 )""")
        
    except:
        conn.rollback()
        a,b,c = sys.exc_info()
        errr = traceback.format_exception(a,b,c)
        for d in errr:
            if(str(d).find("sqlite3.IntegrityError: UNIQUE constraint failed:")>=0):
                l = d.replace("sqlite3.IntegrityError: UNIQUE constraint failed: ","")
                if(l[l.find(".")+1:l.find(" ")]=="id"):
                    return({'id':-2,'message':"شماره شناسایی قبلا در سیستم ثبت شده. ",'status':'error'})
                else:
                    if(l[l.find(".")+1:l.find(" ")]=="shomare_amval"):
                        return({'id':-3,'message':"شماره اموال قبلا در سیستم ثبت شده. ",'status':'error'})
                    else:
                        return({'id':-1,'message':"خطا ناشناخته رخ داده.. ",'status':'error'})
            else:
                return({'id':-7,'message':errr,'status':'error'})
    conn.commit()
    return({'id':1,'message':"ثبت شد.",'status':'success'})