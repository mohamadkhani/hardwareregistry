import json
import sqlite3
import sys
import time
import traceback
import auth

import jdatetime
from bottle import route, request


def _init_(_conn):
    global conn
    conn = _conn


@route('/delete/Hardware', method='POST')
def del_hardware():
    m = json.loads(request.body.read())
    c = conn.cursor()

    c.execute("SELECT id FROM Hardware_Yegan WHERE Hardware_id = " + str(m['shomareAmval']) + ' AND status = 1')
    if len(c.fetchall()) > 0:
        return {'id': -1, 'message': "حذف نشد. یگان با این سخت‌افزار موجود است.", 'status': 'warning'}
    c.execute("SELECT type FROM Hardware WHERE shomareAmval=" + str(m['shomareAmval']))
    a = c.fetchall()
    if a[0][0] == 2:
        c.execute("SELECT shomareAmval FROM `Case` WHERE Monitor_shomareAmval = " + str(m['shomareAmval']))
        b = c.fetchall()
        if len(b) > 0:
            return {'id': -1, 'message': "این مانیتور برای یک سیستم با شماره اموال " + str(b[0][0]) + "  ثبت شده است",
                    'status': 'warning'}
    if a[0][0] == 1:
        conn.execute("DELETE FROM `Case` WHERE shomareAmval = " + str(m['shomareAmval']))
    conn.execute("DELETE FROM Hardware WHERE shomareAmval = " + str(m['shomareAmval']))
    conn.commit()
    return {'id': 1, 'message': "حذف شد.", 'status': 'success'}


@route('/outDate/Hardware', method='POST')
def out_date():
    m = json.loads(request.body.read())

    c = conn.cursor()
    c.execute("SELECT type FROM Hardware WHERE shomareAmval=" + str(m['id']))
    a = c.fetchall()
    if a[0][0] == 2:
        c.execute("SELECT shomareAmval FROM `Case` WHERE Monitor_shomareAmval = " + str(m['id']))
        b = c.fetchall()
        if len(b) > 0:
            return {'id': -1, 'message': "این مانیتور برای یک سیستم با شماره اموال " + str(b[0][0]) + "  ثبت شده است",
                    'status': 'warning'}
    if a[0][0] == 1:
        conn.execute("UPDATE `Case` SET Monitor_shomareAmval = NULL, ip = NULL WHERE shomareAmval = " + str(m['id']))
        conn.commit()
    query = """UPDATE Hardware_Yegan 
    SET status = 0,outDate = """ + str(time.time()) + """ 
    WHERE status = 1 AND Hardware_id = """ + str(m["id"])
    try:
        conn.execute(query)
        query = "UPDATE Hardware SET status = 3 WHERE shomareAmval = " + str(m["id"])
        conn.execute(query)
        conn.commit()
    except:
        a, b, c = sys.exc_info()
        conn.rollback()
        return {'id': -1, 'message': traceback.format_exception(a, b, c), 'status': 'error'}
    return {'id': 1, 'message': "ثبت شد.", 'status': 'success'}


@route('/return/Hardware', method='POST')
def return_hardware():
    m = json.loads(request.body.read())
    c = conn.cursor()
    c.execute("SELECT type FROM Hardware WHERE shomareAmval=" + str(m['id']))
    a = c.fetchall()
    if a[0][0] == 2:
        c.execute("SELECT shomareAmval FROM `Case` WHERE Monitor_shomareAmval = " + str(m['id']))
        b = c.fetchall()
        if len(b) > 0:
            return {'id': -1, 'message': "این مانیتور برای یک سیستم با شماره اموال " + str(b[0][0]) + "  ثبت شده است",
                    'status': 'warning'}
    if a[0][0] == 1:
        conn.execute("UPDATE `Case` SET Monitor_shomareAmval = NULL, ip = NULL WHERE shomareAmval = " + str(m['id']))
        conn.commit()

    query = """UPDATE Hardware_Yegan SET status = 0,outDate = """ + str(
        time.time()) + """ WHERE status = 1 AND Hardware_id = """ + str(m["id"])
    try:
        conn.execute(query)
        query = "UPDATE Hardware SET status = 2 WHERE shomareAmval = " + str(m["id"])
        conn.execute(query)
        conn.commit()
    except:
        a, b, c = sys.exc_info()
        conn.rollback()
        return {'id': -1, 'message': traceback.format_exception(a, b, c), 'status': 'error'}
    return {'id': 1, 'message': "ثبت شد.", 'status': 'success'}


@route('/getAll/Hardware', method='POST')
def get_all():
    m = json.loads(request.body.read())
    page = m['page']
    row_limit = m['row_limit']
    where = ""
    if auth.aaa.current_user.role == "user":
        v = auth.aaa.current_user.email_addr
        where += " Yegan.id = '" + v + "' OR `Y1`.id = '" + v + "' OR `Y1`.ParentYegan_id = '" + v + "' OR `Y2`.ParentYegan_id = '" + v + "' OR `Y3`.ParentYegan_id = '" + v + "' OR `Y4`.ParentYegan_id = '" + v + "' "

    if 'filter' in m:
        first = True
        for k, v in m['filter'].items():
            if first:
                where += " `" + k + "` = '" + v + "' "
                first = False
                if k == "Yegan_id":
                    where += " OR `Y1`.id = '" + v + "' OR `Y1`.ParentYegan_id = '" + v + "' OR `Y2`.ParentYegan_id = '" + v + "' OR `Y3`.ParentYegan_id = '" + v + "' OR `Y4`.ParentYegan_id = '" + v + "'"

            else:
                where += " AND `" + k + "` = '" + v + "' "
                if k == "Yegan_id":
                    where += " OR `Y1`.id = '" + v + "' OR `Y1`.ParentYegan_id = '" + v + "' OR `Y2`.ParentYegan_id = '" + v + "' OR `Y3`.ParentYegan_id = '" + v + "' OR `Y4`.ParentYegan_id = '" + v + "'"

    c = conn.cursor()
    query = """
              FROM `Hardware`
              LEFT JOIN `Hardware_Yegan` ON `Hardware_Yegan`.`Hardware_id` = `shomareAmval`
              LEFT JOIN `Yegan` ON `Hardware_Yegan`.`Yegan_id` = `Yegan`.`id`
              LEFT JOIN `Yegan` Y1 ON `Yegan`.ParentYegan_id = Y1.id
              LEFT JOIN `Yegan` Y2 ON `Y1`.`ParentYegan_id` = Y2.id
              LEFT JOIN `Yegan` Y3 ON `Y2`.`ParentYegan_id` = Y3.id
              LEFT JOIN `Yegan` Y4 ON `Y3`.`ParentYegan_id` = Y4.id 
                       LEFT JOIN Model ON Model_id = Model.id"""
    if where != "":
        query += " WHERE " + where
    query += " GROUP BY `shomareAmval`"
    query += "ORDER BY regDate DESC"
    print(query)
    c.execute("""SELECT 1 """ + query)
    num_rows = len(c.fetchall())


    c.execute("""SELECT 
    `shomareAmval`,`shomareShenasaei`,Hardware.`type`,`Yegan`.`title`,`regDate`,`Hardware`.`status`,
    `Hardware_Yegan`.`status`,`Hardware_Yegan`.`inDate`,`Hardware_Yegan`.`outDate`,`description` ,Model.`title`,`Y2`.title , `Y1`.title"""+query+" LIMIT " + str((page - 1) * int(row_limit)) + ", " + str(row_limit))
    a = c.fetchall()
    oa = []
    type = ""
    for i in a:
        if i[2] == 1:
            type = "کیس"
        if i[2] == 2:
            type = "مانیتور"
        if i[2] == 3:
            type = "پرینتر"
        if i[2] == 4:
            type = "اسکنر"
        if i[2] == 5:
            type = "UPS"
        out_date = "-"
        if i[8] != 0:
            out_date = jdatetime.datetime.fromtimestamp(i[8]).strftime("%y/%m/%d %H:%M")
        oa.append({'shomareAmval': i[0],
                   'shomareShenasaei': i[1],
                   'type': type, 'Yegan_title': i[3],
                   'Model_title':i[10],
                   'regDate': jdatetime.datetime.fromtimestamp(i[4]).strftime("%y/%m/%d %H:%M"),
                   'Hardware_status': i[5],
                   'Hardware_Yegan_status': i[6],
                   'Hardware_Yegan_inDate': jdatetime.datetime.fromtimestamp(i[7]).strftime("%y/%m/%d %H:%M"),
                   'Hardware_Yegan_outDate': out_date,
                   'description': i[9],
                   'Yegan2_title': i[11],
                   'Yegan3_title': i[12]
                   })

    return json.dumps({"rows": oa, 'num_rows': num_rows})


@route("/register/Hardware" , method="POST")
def register_hardware():
    m = json.loads(request.body.read())
    c = conn.cursor()
    c.execute("SELECT * FROM `Hardware` WHERE `shomareAmval` = " + m['shomareAmval'] + "")
    if len(c.fetchall()) == 1:
        return {'id': -5, 'message': "شماره اموال در سامانه موجود است.", 'status': 'error'}
    conn.execute("""
            INSERT INTO `Hardware_Yegan`
            (`Hardware_id`,`Yegan_id`,`inDate`,`outDate`,`status`) VALUES
            (""" + m['shomareAmval'] + """,""" + m['Yegan_id'] + """,""" + str(time.time()) + """,0,1)
            """)
    if "description" not in m.keys():
        m['description'] = ""
    conn.execute("INSERT INTO Hardware (shomareAmval, type, regDate, status, description,Model_id)VALUES ("
                 + m["shomareAmval"] + ","
                 + m['type'] + ","
                 + str(time.time()) + ", 1,'"
                 + m['description'] + "',"
                 + m["Model_id"]
                 + ")")
    conn.commit()
    return {'id': 1, 'message': "ثبت شد.", 'status': 'success'}

