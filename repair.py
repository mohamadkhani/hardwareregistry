import json
import sqlite3
import time

from bottle import post, request
import jdatetime


def _init_(_conn):
    global conn
    conn = _conn

@post('/get/Repair')
def get_repair():
    p_objects = json.loads(request.body.read())
    query = """SELECT id,Case_shomareAmval,description,CPU,CPU_id,Harddisk,Harddisk_id,OS,OS_id,RAM,RAM_id,antivirus,
    setIP,ip,software,inDate,outDate,deliverer,getter,Mainboard_id,Mainboard,power,readyDate,Repairer_id,plumpNum
    FROM Repair WHERE id = """+str(p_objects['id'])
    c = conn.cursor()
    c.execute(query)
    obj = c.fetchall()[0]
    readyDate = 0
    outDate = 0
    if obj[22] is not None:
        readyDate = jdatetime.datetime.fromtimestamp(obj[22]).strftime("%Y/%m/%d %H:%M")
    if obj[16] is not None:
        outDate = jdatetime.datetime.fromtimestamp(obj[16]).strftime("%Y/%m/%d %H:%M")

    return json.dumps({"id": obj[0], "Case_shomareAmval": obj[1],"description": obj[2],"CPU": obj[3]==1, "CPU_id": str(obj[4]),
                       "Harddisk": obj[5]==1, "Harddisk_id": str(obj[6]), "OS": obj[7]==1, "OS_id": str(obj[8]), "RAM": obj[9]==1,
                       "RAM_id": str(obj[10]), "antivirus": obj[11]==1, "setIP": obj[12]==1, "ip": obj[13], "software": obj[14]==1,
                       "inDate": obj[15], "outDate": outDate, "deliverer": obj[17], "getter": obj[18],
                       "Mainboard_id": str(obj[19]), "Mainboard": obj[20]==1, "power": obj[21]==1, "readyDate": readyDate,
                       "Repairer_id": str(obj[23]), "plumpNum": obj[24]})

@post('/getAll/Repair')
def get_all_repair():
    p_objects = json.loads(request.body.read())
    page = p_objects['page']
    row_limit = p_objects['row_limit']
    where = ""
    if "filter" in p_objects and len(p_objects["filter"]) > 0:
        where = " WHERE "
        first = True

        for key, value in p_objects['filter'].items():

            if key == "fromDate":
                if str(value) != "":
                    if not first:
                        where += " AND "
                    first = False
                    date = str(value).split("/")
                    date = jdatetime.datetime(int(date[0]), int(date[1]), int(date[2])).togregorian().timestamp()
                    where += "Repair.`inDate` >= " + str(date) + ""

            elif key == "toDate":
                if str(value) != "":
                    if not first:
                        where += " AND "
                    first = False
                    date = str(value).split("/")
                    date = jdatetime.datetime(int(date[0]), int(date[1]), int(date[2]), 23, 59, 59).togregorian().timestamp()
                    where += "Repair.`inDate` <= " + str(date) + ""

            else:
                if not first:
                    where += " AND "
                first = False
                if key == "Yegan_id":
                    where += " (Yegan.id = '" + str(value) + "' OR `Y1`.id = '" + str(value) + "' OR `Y2`.id = '" + str(value) + "' OR `Y3`.id = '" + str(value) + "' OR `Y4`.id = '" + str(value) + "')"
                elif type(value) == str and value != 'nul':
                    where += "`" + key + "` = '" + str(value) + "'"
                elif value == 'nul':
                    where += "`" + key + "` IS NULL"
                elif value is not None and value != '':
                    where += "`" + key + "` = " + str(value) + ""

    query = """
        FROM Repair
        LEFT JOIN `CPU` ON CPU.id = Repair.CPU_id
        LEFT JOIN Harddisk ON Harddisk.id = Repair.Harddisk_id
        LEFT JOIN OS ON OS.id = Repair.OS_id
        LEFT JOIN RAM ON RAM.id = Repair.RAM_id
        LEFT JOIN Mainboard ON Mainboard.id = Repair.Mainboard_id
        LEFT JOIN `Case` ON `Case`.shomareAmval = Case_shomareAmval
        LEFT JOIN Hardware_Yegan ON Hardware_Yegan.Hardware_id = Case_shomareAmval AND `Hardware_Yegan`.`status` = 1
        LEFT JOIN `Yegan` ON `Yegan`.id = `Hardware_Yegan`.`Yegan_id`
        LEFT JOIN `Yegan` Y1 ON `Yegan`.ParentYegan_id = Y1.id
        LEFT JOIN `Yegan` Y2 ON `Y1`.`ParentYegan_id` = Y2.id
        LEFT JOIN `Yegan` Y3 ON `Y2`.`ParentYegan_id` = Y3.id
        LEFT JOIN `Yegan` Y4 ON `Y3`.`ParentYegan_id` = Y4.id
        LEFT JOIN `Repairer` ON Repair.Repairer_id = Repairer.id
        """ + where + """ GROUP BY `Repair`.id ORDER BY `Repair`.inDate DESC"""
    c = conn.cursor()
    print(query)

    c.execute("""SELECT 1 """+query)
    num_rows = len(c.fetchall())

    #query =  + query
    #query += " LIMIT " + str((page - 1) * int(row_limit)) + ", " + str(row_limit)

    c.execute("""SELECT `Case_shomareAmval`, `Repair`.inDate,`Repair`.outDate , `Repair`.`description`, `CPU`.title, `Harddisk`.title, `OS`.title,
        `RAM`.title, `antivirus`, `Repair`.`ip`, `software`,`deliverer`,`getter`,`Mainboard`.`title`,`power`,`Case`.shomareShenasaei,`Repair`.id,`Repairer`.title,plumpNum
        """+query+" LIMIT " + str((page - 1) * int(row_limit)) + ", " + str(row_limit))
    all = c.fetchall()
    ball = []

    for a in all:
        b = []
        i = 0
        for e in a:
            if i == 1 or i == 2:
                if a[i]:
                    b.append(jdatetime.datetime.fromtimestamp(a[i]).strftime("%y/%m/%d %H:%M"))
                else:
                    b.append(a[i])
            else:
                b.append(e)
            i += 1
        ball.append(b)
    return json.dumps({"rows": ball, 'num_rows': num_rows})

@post('/update/Repair')
def update_repair():
    objects = json.loads(request.body.read())

    updateRepair = " outDate = '" + str(time.time()) + "' "
    updateRepair += ",Repairer_id='" + objects['Repairer_id'] + "' "
    if "RAM" in objects and objects["RAM"] != '0' and objects['RAM']:

        updateRepair += ",`RAM`= '1'"
        updateRepair += ",`RAM_id`=" + objects["RAM_id"]
    if "CPU" in objects and objects["CPU"] != '0' and objects['CPU']:
        updateRepair += ",`CPU`= '1'"
        updateRepair += ",`CPU_id`=" + objects["CPU_id"]

    if "Harddisk" in objects and objects["Harddisk"] != '0' and objects['Harddisk']:
        updateRepair += ",`Harddisk`= '1'"
        updateRepair += ",`Harddisk_id`=" + objects["Harddisk_id"]

    if "Mainboard" in objects and objects["Mainboard"] != '0' and objects['Mainboard']:
        updateRepair += ",`Mainboard`= '1'"
        updateRepair += ",`Mainboard_id`=" + objects["Mainboard_id"]

    if "OS" in objects and objects['OS'] != '0' and objects['OS']:
        updateRepair += ",`OS`= '1'"
        updateRepair += ",`OS_id`=" + objects["OS_id"]

    if "antivirus" in objects:
        if objects["antivirus"]:
            updateRepair += ",`antivirus`= '1'"
    if "power" in objects:
        if objects["power"]:
            updateRepair += ",`power`= '1'"
    if "setIP" in objects and objects["ip"] is not None:
        updateRepair += ",`setIP`= '1'"
        updateRepair += ",`ip`='" + objects["ip"] + "'"

    if "software" in objects:
        if objects["software"]:
            updateRepair += ",`software`= '1'"

    if "description" in objects and objects["getter"] is not None:
        if objects["description"] != '':
            updateRepair += ",`description`= '" + objects["description"] + "' "
    if "getter" in objects and objects["getter"] is not None:
        if objects["getter"] != '':
            updateRepair += ",`getter`= '" + objects["getter"] + "' "

    if "outDate" in objects and objects["outDate"] is not None:
        date = str(objects["outDate"]).split(" ")[0].split("/")
        t = str(objects["outDate"]).split(" ")[1].split(":")
        date = jdatetime.datetime(int(date[0]), int(date[1]), int(date[2]), int(t[0]), int(t[1])).togregorian().timestamp()
        #date.time(int(t[0]), int(t[1]))
        #date.hour = int(t[0])
        #date.minute = int(t[1])
    if "readyDate" in objects and objects["readyDate"] is not None:
        date = str(objects["readyDate"]).split(" ")[0].split("/")
        t = str(objects["readyDate"]).split(" ")[1].split(":")
        date = jdatetime.datetime(int(date[0]), int(date[1]), int(date[2]), int(t[0]),
                                  int(t[1])).togregorian().timestamp()

        updateRepair += ", `readyDate`  = " + str(date) + " "
    if "plumpNum" in objects and objects["plumpNum"] is not None:
        if objects["plumpNum"] != '':
            updateRepair += ",`plumpNum`= " + objects["plumpNum"] + " "
    print("UPDATE Repair Set "+updateRepair+" WHERE id = "+str(objects["id"]))
    conn.execute("UPDATE Repair Set "+updateRepair+" WHERE id = "+str(objects["id"]))


@post('/register/Repair')
def register_repair():
    objects = json.loads(request.body.read())
    c = conn.cursor()
    c.execute("SELECT id FROM Repair WHERE Case_shomareAmval=" + objects['Case_shomareAmval'] + " AND outDate is NULL")
    if len(c.fetchall()) == 0:
        return {'id': -1, 'message': "سیستم در حال تعمیر نیست.", 'status': 'warning'}

    values = objects['Case_shomareAmval']
    keys = "`Case_shomareAmval`"

    update = "`shomareAmval`=" + objects['Case_shomareAmval']
    updateRepair = " outDate = '"+str(time.time())+"' "
    updateRepair += ",Repairer_id='" + objects['Repairer_id'] + "' "
    if "RAM" in objects and objects["RAM"] != '0':
        update += ",`RAM_id`=" + objects["RAM_id"]
        updateRepair += ",`RAM`= '1'"
        updateRepair += ",`RAM_id`=" + objects["RAM_id"]
    if "CPU" in objects and objects["CPU"] != '0':
        updateRepair += ",`CPU`= '1'"
        updateRepair += ",`CPU_id`=" + objects["CPU_id"]
        update += ",`CPU_id`=" + objects["CPU_id"]
    if "Harddisk" in objects and objects["Harddisk"] != '0':
        updateRepair += ",`Harddisk`= '1'"
        updateRepair += ",`Harddisk_id`=" + objects["Harddisk_id"]
        update += ",`Harddisk_id`=" + objects["Harddisk_id"]
    if "Mainboard" in objects and objects["Mainboard"] != '0':
        updateRepair += ",`Mainboard`= '1'"
        updateRepair += ",`Mainboard_id`=" + objects["Mainboard_id"]
        update += ",`Mainboard_id`=" + objects["Mainboard_id"]
    if "OS" in objects and objects['OS'] != '0':
        updateRepair += ",`OS`= '1'"
        updateRepair += ",`OS_id`=" + objects["OS_id"]
        update += ",`OS_id`=" + objects["OS_id"]
    if "antivirus" in objects:
        if objects["antivirus"]:
            updateRepair += ",`antivirus`= '1'"
    if "power" in objects:
        if objects["power"]:
            updateRepair += ",`power`= '1'"
    if "setIP" in objects:
        updateRepair += ",`setIP`= '1'"
        updateRepair += ",`ip`='" + objects["ip"] + "'"
        update += ",`ip`='" + objects["ip"] + "'"
    if "software" in objects:
        if objects["software"]:
            updateRepair += ",`software`= '1'"

    if "description" in objects:
        if objects["description"] != '':
            updateRepair += ",`description`= '"+objects["description"]+"' "
    if "getter" in objects:
        if objects["getter"] != '':
            updateRepair += ",`getter`= '"+objects["getter"]+"' "
    if "plumpNum" in objects and objects["plumpNum"] is not None:
        updateRepair += ",`plumpNum`= " + str(objects["plumpNum"]) + " "
    keys += ", `outDate` "
    values += ", " + str(time.time()) + " "
    print("""    UPDATE Repair SET """ + updateRepair + """ WHERE is_null(outDate) AND Case_shomareAmval =""" + objects[
        'Case_shomareAmval']
          )
    conn.execute("""
    UPDATE Repair SET """+updateRepair+""" WHERE outDate is NULL AND Case_shomareAmval ="""+objects['Case_shomareAmval']+" "
    )

    updQuery = "UPDATE `Case` SET "+update+" WHERE `shomareAmval`="+objects['Case_shomareAmval']
    print(updQuery)
    conn.execute(updQuery)
    conn.commit()
    return {'id': 1, 'message': "ثبت شد.", 'status': 'success'}


@post('/register/RepairInit')
def register_repair_init():
    objects = json.loads(request.body.read())

    if "deliverer" not in objects:
        return {'id': 1, 'message': "تحویل دهنده معین نشده است.", 'status': 'warning'}

    c = conn.cursor()
    c.execute("SELECT id FROM Repair WHERE Case_shomareAmval="+objects['Case_shomareAmval']+" AND outDate is NULL")
    if len(c.fetchall()) > 0:
        return {'id': -1, 'message': "سیستم هم اکنون در حال تعمیر است.", 'status': 'warning'}
    conn.execute("""
        INSERT INTO Repair 
        (Case_shomareAmval,inDate,deliverer) 
        VALUES ("""+str(objects['Case_shomareAmval'])+","+str(time.time())+",'"+str(objects['deliverer'])+"')")
    conn.commit()
    return {'id': 1, 'message': "ثبت شد.", 'status': 'success'}

